s.boot;
(
~out=Bus.audio(s,2);


//Synths

SynthDef(\pad,{arg out=0,f=#[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],d=0,a=0.1,amp=0.11;
    var sig=0;
    var sig2=SinOsc.ar(f[15],0,0.08);
    var env=EnvGen.kr(Env.linen(6,7,10),doneAction:2);
    var amplitude;
    var trig;
    var trigCon;
    var timer;

    sig=Array.fill(16,{|n| [SinOsc.ar(f[n]+SinOsc.ar(f[n]*10,0,d),0,a),VarSaw.ar(f[n]+SinOsc.ar(f[n]*10,0,d),0,0.5,a),LFTri.ar(f[n]+SinOsc.ar(f[n]*10,0,d),0,a),WhiteNoise.ar(0.001)].wchoose([0.33,0.33,0.33,0.01])});
    sig=Splay.ar(sig,0.5);

    amplitude=Amplitude.kr((sig+sig2)*env);
    trig=amplitude > amp;
    timer=Timer.kr(trig);
    trigCon=(timer>4)*trig;
    SendTrig.kr(trigCon,0,Demand.kr(trigCon,0,Dseq([0,1])));

    Out.ar(out,((sig+sig2)*env)!2);
}).add;

SynthDef(\out,{arg out=0,f=0;
    var in=In.ar(~out,2);
    in=RLPF.ar(in,(8000+f)*LFTri.kr(0.1).range(0.5,1),0.5);
    in=RLPF.ar(in,(4000+f)*LFTri.kr(0.2).range(0.5,1),0.5);
    in=RLPF.ar(in,(10000+f)*LFTri.kr(0.24).range(0.5,1),0.5);
    in=in*0.5 + CombC.ar(in*0.5,4,1,14);
    in=in*0.99 + (in*SinOsc.ar(10,0,1)*0.01);
    Out.ar(out,Limiter.ar(in,0.9));
}).add;

//Octaves
a=[72,76,79,83];
b=a-12;
c=b-12;
d=c-12;

//Function that will listen to OSC messages and allocate synths

o = OSCFunc({ arg msg, time;
    [time, msg].postln;
    if(msg[3] == 1,{Synth(\pad,[\f:([a.choose,b.choose] ++ Array.fill(5,{b.choose}) ++ Array.fill(5,{c.choose}) ++ Array.fill(4,{d.choose})).midicps,\a:0.1/rrand(2,4),\d:rrand(0,40),\amp:rrand(0.10,0.115),\out:~out]);});
},'/tr', s.addr);
)

//Main out
y=Synth(\out);

Synth(\pad,[\f:([a.choose,b.choose] ++ Array.fill(5,{b.choose}) ++ Array.fill(5,{c.choose}) ++ Array.fill(4,{d.choose})).midicps,\a:0.1/rrand(2,4),\d:rrand(0,40),\amp:rrand(0.10,0.115),\out:~out]);


o.free;
y.free;


//Original arduino noise

(
//min 40, max 900
~synth = {
	arg freq=1000;
	var sig;
	sig = WhiteNoise.ar(1!2);
	sig =LPF.ar(sig, freq, 0.2);
}.play
)

//Arduino noise part simulation /w mouse

{
        var x = SinOsc.ar(MouseX.kr(1, 100));
        SinOsc.ar(300 * x + 800, 0, 0.1)
        +
        PinkNoise.ar(0.1 * x + 0.1)
}.play;

{ SinOsc.ar(MouseButton.kr(400, 440, 0.1), 0, 0.1) }.play;
{ SinOsc.ar(MouseButton.kr(400, 740, 2), 0, 0.1) }.play;

{ PinkNoise.ar(MouseY.kr(0.1), 0, 0.1) }.play;
{ PinkNoise.ar(MouseX.kr(0.1), 0, 0.1) }.play;



(
SynthDef( \mousexyb, { |out=0|
    var mousex, mousey, mousebutton;
    mousex = MouseX.kr( 500, 1000 );
    mousey = MouseY.kr( 0, 0.3 );
    mousebutton = MouseButton.kr( 0, 1, 2 );
    Out.ar( out, SinOsc.ar( mousex, 0, mousey ) * mousebutton );
}).add
)

x = Synth.new( \mousexyb );
x.free;

s.quit;